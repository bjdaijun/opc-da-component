组件结构

![输入图片说明](https://images.gitee.com/uploads/images/2021/0610/115514_2e6a8e6a_1147276.png "屏幕截图.png")

1、本opc驱动采用订阅模式来读写OPC
2、通过订阅MQTT来接收外部命令以及将读到的数据写出


使用说明：

第一步：安装好MQTT，任意OPC服务器，配置好相应的Tags
  本例子假设你已经配置了如下TAG
  

第二步：引用的关键的类库
  首先需要把OpcDriver目录下OPCDAAuto.dll放到System32下，然后用管理员运行cmd，
  执行 regsvr32 OPCDAAuto.dll 注册，之后通过引用OCM中的OPC DA Automaition Wrapper 来使用。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0610/153829_61c23446_1147276.png "屏幕截图.png")


第三步：项目在运行前，须配置好app.config
   
      <!--mqtt配置-->
    <add key="mqtt.url" value="192.168.31.41"/>
    <add key="mqtt.clientId" value="opc-1"/>
    <add key="mqtt.port" value="1883"/>
    <add key="mqtt.topic.opc.receive" value="opc/receive"/>   //用来接收外部命令
    <add key="mqtt.topic.opc.publish" value="opc/publish"/>   //用来写出读到的数据
   
    <!--opc点表配置-->
    <add key="opc.serverName" value="Kepware.KEPServerEX.V6"/>  //OPC 服务器名，本文用的是kepwareEXV6
    <add key="opc.group" value="通道"/>   //随便写
    <add key="opc.tags" value="通道1.Device2.A1,通道1.Device2.A2"/>   //配置标记，每个标记用逗号隔开，必须在opc服务器中找到对应的tag，否则运行时会报异常。

外部语言调用：

1、 **写数据** ：向opc/receive 发送以下内容
   {"通道1.Device2.A1":1,"通道2.Device2.A2":0}
    上面的格式是Map<string,string>, key是点表，value是要写入的值

2、 **读数据** :从opc/publish获取内容
  {"通道1.Device2.A1":1,"通道2.Device2.A2":0}
    key是点表，value是读到的值

注意、 当程序启动时，会自动读取点表所有的当前值，之后只会在值发生变化时才会读到并自动发送给MQTT
