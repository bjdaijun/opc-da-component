﻿
引用的类库：
  首先需要把目录下OPCDAAuto.dll放到System32下，然后用管理员运行cmd，  regsvr32 OPCDAAuto.dll

1、本opc驱动采用订阅模式来读写OPC
2、通过订阅MQTT来接收外部命令以及将读到的数据写出
3、项目在运行前，须配置好app.config
   
      <!--mqtt配置-->
    <add key="mqtt.url" value="192.168.31.41"/>
    <add key="mqtt.clientId" value="opc-1"/>
    <add key="mqtt.port" value="1883"/>
    <add key="mqtt.topic.opc.receive" value="opc/receive"/>   //用来接收外部命令
    <add key="mqtt.topic.opc.publish" value="opc/publish"/>   //用来写出读到的数据
    <!--opc点表配置-->

    <add key="opc.serverName" value="Kepware.KEPServerEX.V6"/>
    <add key="opc.group" value="通道"/>   //随便写
    <add key="opc.tags" value="通道1.Device2.A1,通道1.Device2.A2"/>   //必须在opc服务器中找到对应的tag


4、输入命令格式
   {"通道1.Device2.A1":1,"通道2.Device2.A2":0}
    上面的格式是Map<string,string>, key是点表，value是要写入的值

5、输出内容格式
  {"通道1.Device2.A1":1,"通道2.Device2.A2":0}
    key是点表，value是读到的值

6、 当程序启动时，会自动读取点表所有的当前值，之后只会在值发生变化时才会读到并自动发送给MQTT
